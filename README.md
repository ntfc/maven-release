# Step 1 - Start Nexus

To start using this demo, you need a working nexus repository working locally:
```
$ cd nexus/
$ docker-compose up -d
```

You will now have a nexus repository available at http://localhost:8081/nexus
(`admin`/`admin123`)

# Step 2 - Create a new release

First of all, for this POC we need to use a wrapper around 
`mvn` because we are using a `settings.xml` that points to
a local nexus repository. In your usual world this would all
be stored on a global `~/.m2/settings.xml`


```
$ ./mvn.sh release:prepare
$ ./mvn.sh release:perform
$ ./mvn.sh release:rollback
````

